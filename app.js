const express = require('express');
const path = require('path');
const fs = require('fs');
const https = require('https');
const app = express();
const port = 8443;

// Устанавливаем движок шаблонов EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Чтение сертификатов
const options = {
  key: fs.readFileSync(path.join(__dirname, 'certs', 'server.key')),
  cert: fs.readFileSync(path.join(__dirname, 'certs', 'server.cert'))
};

// Маршрут для главной страницы
app.get('/', (req, res) => {
  res.render('index', { title: 'Welcome', message: 'Learn more about Ansible below:', ansibleInfo: 'Ansible is an open-source automation tool used for configuration management, application deployment, and task automation. It allows you to describe your system configuration in simple, human-readable YAML files, making it easy to manage and scale infrastructure.' });
});

// Запуск HTTPS сервера
https.createServer(options, app).listen(port, () => {
  console.log(`App running at https://localhost:${port}`);
}).on('error', (err) => {
  console.error('Failed to start server:', err);
});
