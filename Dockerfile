# Используем официальный образ Node.js в качестве базового
FROM node:14

# Создаем рабочую директорию
WORKDIR /app

# Копируем package.json и package-lock.json
COPY package*.json ./

# Устанавливаем зависимости
RUN npm install

# Копируем исходный код и сертификаты
COPY . .

# Открываем порт для приложения
EXPOSE 8443

# Запускаем приложение
CMD ["node", "app.js"]
